FROM node:carbon-alpine

WORKDIR /usr/src/app

COPY . .

RUN yarn install

EXPOSE 8123

CMD [ "yarn", "start" ]
