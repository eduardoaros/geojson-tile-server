Construir imagen docker 

```bash
docker build --rm -f Dockerfile -t geojson-tile-server:latest .
```

Ejecutar contenedor

```bash
docker run --rm -it -p 8123:8123 geojson-tile-server:latest
```