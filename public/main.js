
    var layers = document.getElementById('layers');

    var map = new ol.Map({
        target: 'map',
        view: new ol.View({
            // projection: 'EPSG:4326',
            // Santiago de Chile [lon, lat]
            center: [-7869956.43, -3957603.58],
            zoom: 10
        }),
        layers: [
            new ol.layer.Tile({
                source: new ol.source.OSM()
            }),
        ]
    });

    var selectPointerMove = new ol.interaction.Select({
        condition: ol.events.condition.pointerMove
    });
    map.addInteraction(selectPointerMove);

    var colors = [
        'indigo', 'green', 'blue', 'red'
    ];
    var styleFunction = function (feature) {
        return new ol.style.Style({
            stroke: new ol.style.Stroke({
                width: 1,
                color: colors[layers.selectedIndex]
            })
        })
    };

    var currentLayer = new ol.layer.VectorTile({
        source: new ol.source.VectorTile({
            format: new ol.format.GeoJSON(),
            url: "/red_drenaje/{z}/{x}/{y}.geojson"
        }),
        style: styleFunction
    });
    map.addLayer(currentLayer);

    function changeLayer(e) {
        currentLayer.set('style', styleFunction);
        currentLayer.setSource(new ol.source.VectorTile({
            format: new ol.format.GeoJSON(),
            url: "/" + this.value + "/{z}/{x}/{y}.geojson"
        }));
    }

    document.getElementById('layers').addEventListener('change', changeLayer);

    var overlay = new ol.Overlay({
        element: document.getElementById('popup-container'),
        positioning: 'bottom-center',
        offset: [0, -10],
        autoPan: true
    });
    map.addOverlay(overlay);

    overlay.getElement().addEventListener('click', function () {
        overlay.setPosition();
    });

    map.on('click', function (e) {
        let markup = '';
        map.forEachFeatureAtPixel(e.pixel, function (feature) {
            markup += `${markup && '<hr>'}<table>`;
            const properties = feature.getProperties();
            for (const property in properties) {
                markup += `<tr><th>${property}</th><td>${properties[property]}</td></tr>`;
            }
            markup += '</table>';
        }, { hitTolerance: 1 });
        if (markup) {
            document.getElementById('popup-content').innerHTML = markup;
            overlay.setPosition(e.coordinate);
        } else {
            overlay.setPosition();
        }
    });
